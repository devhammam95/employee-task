<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
class Employee extends Model
{
    //
    protected $table     = "employees";
    protected $fillable  = ['first_name','last_name','image','job','user_id','status'];
    protected $hidden    = [];
    public    $timestamps= true;

    public function getFullNameAttribute(){
        return ucfirst($this->first_name) .' '. ucfirst($this->last_name);
    }

    public function User(){
        return $this->belongsTo(User::class,'user_id');
    }

    public static function uploadFile($img,$file)
    {
       if(Storage::disk('public')->put($img,file_get_contents($file)))
           return true;
    }

}
