@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login To Dashboard ..</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="{{ route('adminDashboardLogin') }}">
                            {{ csrf_field() }}
                            <fieldset>
                                @if($session=Session::get('alert-danger'))
                                    <div class="alert alert-danger">
                                        <div class="media-body">{{ $session }}</div>
                                    </div>
                               @endif
                                <div class="form-group">
                                    <input input="text"   class="form-control" placeholder="Enter your user-name" name="userName" autofocus>

                                @if ($errors->has('userName'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('userName') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Enter your Password" name="password" >

                                @if ($errors->has('password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                </div>
                                    <input type="submit" value="Login" class="btn btn-lg button-custom btn-block" style="border: solid 1px #4f96c4;">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection