<div class = "navbar-header">
    <button type = "button" class = "navbar-toggle" data-toggle = "collapse" data-target = ".navbar-collapse">
        <span class = "sr-only">Toggle navigation</span>
        <span class = "icon-bar"></span>
        <span class = "icon-bar"></span>
        <span class = "icon-bar"></span>
    </button>
    <a class = "navbar-brand text-center" href = "{{ route('allEmployee') }} "> Employee App Dashboard </a>
</div>
<ul class = "nav navbar-top-links navbar-left">
    <li><a href = "{{ route('adminLogout') }}" >logout</a>
</ul>