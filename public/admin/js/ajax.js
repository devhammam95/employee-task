$(document).ready(function () {
    //St add employee in admin/employees/index
    $('.addEmployee').click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
        var form    = $('.formAdd');
        var formAdd = new FormData(form[0]);
        var route = $(this).attr('data-route');
        $.ajax({
            type: 'post',
            url: route,processData: false, contentType: false, cache: false,
            data:formAdd,
            dataType: 'json',
            success: function (data) {
                if (data.status==true) {
                    form[0].reset();
                    $('#addModal').modal('hide');
                    $('.data-collection').prepend(data.employeeBlock);
                    swal({
                        title: '',
                        text: 'Employee is Added successfully ',
                        timer: 2000,
                        type: 'success',
                    });
                }
                 else {
                 console.log('sdfx');
                 }
            },
            error: function () {
                console.log('sdf');
            }
        });
    });
    //End add Employee in admin/employees/index
    /*****************************************************************/
    //St update Employee in admin/employees/index
    $('.deleteEmployee').click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
        var id = $(this).data('id');
        var route = $(this).data('route');
        swal({
            title: '',
            text: 'Do you want to delete this one ?',
            timer: 1500,
            type: 'warning',
            showCancelButton:true,
            showConfirmButton:true,
        },function (isConfirm) {
            if (isConfirm){
                $.ajax({
                    type: 'post',
                    url: route,
                    data: {id: id},
                    dataType: 'json',
                    success: function (data) {
                        if (data.success==true)
                        {
                            console.log('hit code');
                            // sweet alert
                            $('.item-'+id).addClass('alert alert-danger').fadeOut(800);
                        }
                        swal({
                            title: '',
                            text: ''+ data.msg +'',
                            timer:2000,
                            type: data.success==true?'success':'warning',
                        });
                    }
                });
            }
        });
    });
   //End update Employee in admin/employees/index
   /***********************************************************************/
    $('.editEmployee').click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
        var id = $(this).data('id');
        var route = $(this).attr('data-route');
        if(!id){
            swal({
                title: '',
                text: 'no employee for this ,try again! ',
                timer: 2000,
                type: 'warnging',
            });
        }
        $.ajax({
            type: 'get',
            url: route,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    //var url=('meta[name="url"]').attr('content')
                    var em=data.employee;
                    $('#userImage').attr('src',data.urlImg);
                    $('.firstNameEdit').val(em.first_name);
                    $('.lastNameEdit').val(em.last_name);
                    $('.jobEdit').val(em.job);
                    $('.statusEdit').val(em.status);
                    $('.employeeEditedId').val(em.id);
                }else{
                    swal({
                        title: '',
                        text: ''+data.msg+'',
                        timer: 1500,
                        type: 'warning',
                    });
                }
            },
        });
    });
    });
       /*****************************************************/
$('.updateEmployee').click(function (e) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    e.preventDefault();
    var form    = $('.formEdit');
    var formEdit = new FormData(form[0]);
    var route = $(this).data('route');
    $.ajax({
        type: 'post',
        url: route,
        data: formEdit,processData: false, contentType: false, cache: false,
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (data.status==true)
            {
                form[0].reset();
                $('#editModal').modal('hide');
                var em=data.employee;
                var emId=em.id;
                console.log(emId);
                $('.td-img-'+emId).attr('src',''+em.image+'');
                $('.td-fullName-'+emId+'').html('').html(em.first_name+' '+em.last_name);
                $('.td-job-'+emId).text(em.job);
                $('.td-status-'+emId).text(em.status);
                $('.td-userName-'+emId).text(em.user.user_name);
                $('.item-'+emId).addClass('alert alert-success');
            }
            swal({
                title: '',
                text: ''+ data.msg +'',
                timer: 1500,
                type: data.status==true?'success':'warning',
            });
        }
    });
      /* reset forms */
    $('.modal').on('hidden.bs.modal', function(){
        $(this).find('form')[0].reset();
    });
     /****/
});
