<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Routes For Admin Dashboard

// login + logout
    Route::group(['prefix'=>'admin/dashboard','namespace'=>'admin'],function() {
    Route::get('admin-login', 'AdminDashboardController@showLogin')->name('adminLogin');
    Route::post('adminLogin', 'AdminDashboardController@adminLogin')->name('adminDashboardLogin');
    Route::get('admin-logout', 'AdminDashboardController@adminLogout')->name('adminLogout')->middleware('Admin');
    //Route::post('adminLogout', 'AdminDashboardController@logout')->middleware('Admin');
});

Route::group(['prefix'=>'admin','namespace'=>'admin','middleware'=>'Admin'],function() {
        //Routes For Employee
        Route::group(['prefix' => 'employee'], function () {
        Route::get('all', 'EmployeeController@index')->name('allEmployee');
        Route::post('add-employee', 'EmployeeController@store')->name('addEmployee');
        Route::get('edit-employee/{employeeId}', 'EmployeeController@edit')->name('editEmployee');
        Route::post('update-employee', 'EmployeeController@update')->name('updateEmployee');
        Route::post('delete-employee', 'EmployeeController@destroy')->name('deleteEmployee');

    });
});

Route::get('/', function () {
    return view('welcome');
});
