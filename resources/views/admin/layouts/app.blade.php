<!DOCTYPE html>
<html lang = "{{ app()->getLocale() }}">
<head>
    <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE=edge">
    <meta name= "viewport" content = "width=device-width, initial-scale=1">
    <meta name= "description" content = "employee app">
    <meta name= "author" content = "Mohamed Hassan Hammam">
    <meta name= "csrf-token" content = "{{ csrf_token() }}">
    <meta name= "url" content = "{{ url('/') }}">

    <title>Admin Panel</title>
    <link   href=  "{{ asset('admin/css/bootstrap.min.css') }}" rel = "stylesheet">
    <link   href=  "{{ asset('admin/css/font-awesome.min.css') }}" rel = "stylesheet">
    <link   href=  "{{ asset('admin/css/style.css') }}" rel = "stylesheet">
    <link   href=  "{{ asset('admin/css/sweetalert.css') }}" rel = "stylesheet">
    <link rel="stylesheet"
          href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>
<body>
<div id = "wrapper">
    <nav class = "navbar navbar-default navbar-static-top navbar-fixed-top" role = "navigation" style = "margin-bottom: 0">
        <!-- Just For Coding Fast use @ auth -->
        @auth
        @include('admin.layouts.header')
        @endauth
    </nav>

      @yield('content')
</div>
@include('admin.layouts.footer')
</body>
</html>