@extends('admin.layouts.app')
@section('content')
    <div id = "page-wrapper">
        <div class = "container-fluid">
            <div class = "row">
                <div class = "col-lg-12">
                    <h1 class = "page-header"><i class = "fa fa-clone"></i> View/Edit Employees</h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class = "panel panel-default">
                    <div class = "panel-heading">
                        <button class="btn btn-default btn-success" data-toggle="modal" data-target="#addModal">Add Employee</button>
                    </div>
                    <div class = "panel-body">
                        <table width = "100%" id="table" class = "table table-striped table-bordered table-hover datatable" >
                            <thead >
                            <tr>
                                <th>Full Name</th>
                                <th>Image</th>
                                <th>Job</th>
                                <th>UserName</th>
                                <th>Status</th>
                                <th>Operatios</th>
                            </tr>
                            </thead>
                            <tbody class="data-collection">
                            @if(count($employees ))
                            @foreach($employees as $employee)
                            <tr class = "item-{{ $employee->id }}">
                                <td class="td-fullName-{{ $employee->id }}">{{ $employee->full_name }}</td>
                                <td class="td-img-{{ $employee->id }}"><img width="80" height="80" src="{{ asset('storage/'.$employee->image) }}"></td>
                                <td class="td-job-{{ $employee->id }}">{{ substr($employee->job,0,50) }}<span>....</span></td>
                                <td class="td-userName-{{ $employee->id }}">{{ $employee->User->user_name }}</td>
                                <td class="td-status-{{ $employee->id }}">{{ $employee->status }}</td>
                                <td>
                                    <button data-id="{{ $employee->id }}" data-route="{{ route('editEmployee',['employeeId'=>$employee->id]) }}" class = "btn btn-default button-custom editEmployee" data-toggle = "modal" data-target = "#editModal">
                                        Edit
                                    </button>
                                    <button style="color: white"  data-id="{{ $employee->id }}" data-route="{{ route('deleteEmployee') }}" class="deleteEmployee btn btn-default btn-danger ">
                                        delete
                                    </button>
                                </td>
                            </tr>
                                @endforeach

                                 @else
                                  <span class="text-danger lead"> not employees nntil now!</span>
                                 @endif

                            </tbody>
                        </table>
                    </div>
                </div>

                 <!--edit modal-->
                <div class = "modal fade" id = "editModal" role = "form" aria-labelledby = "myModalLabel"
                     aria-hidden = "true">
                    <div class = "modal-dialog  modal-lg">
                        <div class = "modal-content">
                            <div class = "row">
                                <div class = "container-fluid">
                                    <h1 class = "modal-header"><i class = "fa fa-clone"></i> Edit Employee</h1>
                                    <div class = "modal-body">
                                        <div class = "row">
                                            <form role = "form" method="post" enctype="multipart/form-data" class="formEdit" >
                                                        <div class = "col-md-6 col-sm-8">
                                                            <div class = "col-xs-10">
                                                                <img  id="userImage" src="{{ asset('/admin/img/user.png') }}" class="userImageEdit col-xs-12">
                                                                <label class = "btn btn-default button-custom upload-picture col-xs-12">
                                                                    Browse <input  id="uploadFile" name="img" type = "file" class = "hidden">
                                                                </label>
                                                            </div>
                                                        </div>
                                                <div class="form-group col-md-6">
                                                    <label for="firstName" class="text-muted">firstName</label>
                                                    <input type="text" id="firstName" name="firstName" class="form-control firstNameEdit" placeholder="Enter first name" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="lastName" class="text-muted">lastName</label>
                                                    <input type="text" name="lastName" id="lastName"class="form-control lastNameEdit"placeholder="Enter last name"  >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="job" class="text-muted">Job</label>
                                                  <textarea id="job" name="job"class="form-control jobEdit" placeholder="Enter job details" ></textarea>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="status" class="text-muted">status</label>
                                                  <select name="status" id="status" class="statusEdit form-control">
                                                    <option value="active">active</option>
                                                    <option value="not_active">not active</option>
                                                  </select>
                                                </div>
                                                <input type="hidden" value="" name="employeeEditedId" class="employeeEditedId" >
                                                <div class="form-group col-md-6 pull-right">
                                                                    <button data-route="{{ route('updateEmployee') }}"  type="submit" class="updateEmployee btn btn-default col-md-5 button-custom" style="margin-right: 8px;">
                                                                        Update
                                                                    </button>
                                                    <button type="button" class="btnClose btn btn-default btn-warning" data-dismiss="modal">Close</button>
                                                </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row (nested) -->

                                    </div>
                                </div>

                            </div>
                        </div>

                <!--add modal-->
                <div class = "modal fade" id = "addModal" role = "form" aria-labelledby = "myModalLabel"
                     aria-hidden = "true">
                    <div class = "modal-dialog  modal-lg">
                        <div class = "modal-content">
                            <div class = "row">
                                <div class = "container-fluid">

                                    <h1 class = "modal-header"><i class = "fa fa-clone"></i> Add Employee</h1>

                                    <div class = "modal-body modalAdd">
                                        <div class = "row">
                                            <form role = "form" method="post" class="formAdd" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class = "col-md-6 col-xs-10">
                                                    <img  id="userImage" src="{{ asset('/admin/img/user.png') }}" class="user-image col-xs-12">
                                                    <label class = "btn btn-default button-custom upload-picture col-xs-12">
                                                        Browse <input  id="file_uploadImg" name="img" type = "file" class = "hidden imgUserFileAdd"  >
                                                    </label>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="firstName" class="text-muted">firstName</label>
                                                    <input type="text" id="firstName" name="firstName" class="form-control firstNameAdd" placeholder="Enter first name" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="lastName" class="text-muted">lastName</label>
                                                    <input type="text" name="lastName" id="lastName"class="form-control lastNameAdd"placeholder="Enter last name"  >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="job" class="text-muted">Job</label>
                                                    <textarea name="job" id="job" class="form-control jobAdd" placeholder="Enter job details" ></textarea>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="status" class="text-muted">status</label>
                                                    <select name="status" id="status" class="statusAdd form-control">
                                                        <option value="active">active</option>
                                                        <option value="not_active">not active</option>
                                                    </select>
                                                </div>
                                                @php
                                                $users=\App\User::where('user_name','!=','admin95')->get();
                                                @endphp
                                                <div class="form-group col-md-6">
                                                    <label for="userId" class="text-muted">userName</label>
                                                    <select name="userId" id="userId" class="userId form-control">
                                                        @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->user_name }}</option>
                                                            @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-6 pull-right">
                                                    <input data-route="{{ route('addEmployee') }}"  value="Add" type = "submit" class="addEmployee btn btn-default col-md-5 button-custom"  style="margin-right:8px;">
                                                    <button type="button" class="btn btn-default btn-warning btnClose " data-dismiss="modal">Close</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
                </div>
            </div>

        </div>
    </div>

@endsection