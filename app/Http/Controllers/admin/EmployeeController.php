<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Employee;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Employee $model)
    {
        $this->view='admin/employees/';
        $this->model=$model;
    }
    public function index()
    {
        $employees=$this->model->orderBy('created_at','Desc')
                               ->get();
        return view($this->view.'index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $image   = $request->file('img');
        $imageUploaded=$image->getClientOriginalName();
        //return $request;
        $employee = new Employee();
        $employee->first_name = $request->firstName;
        $employee->last_name  = $request->lastName;
        $employee->user_id    =  $request->userId;
        $employee->job        = $request->job;
        $employee->status     = $request->status;
        $employee->image      = $imageUploaded;
        $employee->save();
        $v=Employee::uploadFile($imageUploaded,$image);
        if ($v==true)
        {
            $employeeBlock=view($this->view.'employee-block',compact('employee'))
                ->render();
            return response()->json(['status'=>true,'employeeBlock'=>$employeeBlock]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee=Employee::find($id);
        if($employee){
        return response()->json(['status'=>true,'employee'=>$employee,'urlImg'=>Storage::url($employee->image)]);
        }
        return response()->json(['status'=>false,'msg'=>'no employee for this']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {    //return response()->json(['status'=>true,'r'=>$request->all()]);
        if ($image=$request->hasFile('img')){
            $imageUploaded=$image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
        }
        //return $request;
        $employee = Employee::with('User')->find($request->employeeEditedId);
        $employee->first_name = $request->firstName;
        $employee->last_name  = $request->lastName;
        //$employee->user_id  = $request->userId;
        $employee->job        = $request->job;
        $employee->status     = $request->status;
        if ($image) {
            $v = Employee::uploadFile($image,$imageUploaded);
            $employee->image  = $imageUploaded;
        }
        $employee->save();
        return response()->json(['status'=>true,'msg'=>'Updated Successfully','employee'=>$employee]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $employee=Employee::find($request->id);
        if ($employee)
        {
          $employee->delete();
          return response()->json([
                                   'success' => true,
                                   'msg'     => 'deleted successfully'
                                  ]);
        }
          return response()->json([
                               'success'=> false,
                               'msg'    => 'not found'
                             ]);

    }
}
