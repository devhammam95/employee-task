<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminLoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class AdminDashboardController extends Controller
{
    //
    public function showLogin(){
        Auth::user()&&Auth::user()->user_name=='admin95' ? $view=redirect()->route('allEmployee'): $view=view('admin.login');
        return $view;
    }

    public function adminLogin(AdminLoginRequest $request)
    {
        $credentials=[
                       'user_name' => $request['userName'],
                       'password'  => $request['password'],
                     ];
        if(Auth::attempt($credentials))
        {
            return redirect()->route('allEmployee');
        }else{
            return redirect()->route('adminLogin')->with('alert-danger','userName or password not matched ,try again plz !');
        }
    }

    public function adminLogout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect()->route('adminLogin');
    }

}
