<footer>
    <p>&copy; Employee App {{ date('Y') }} </p>
</footer>
</div>
<!-- jQuery -->
<script src = "{{ asset('admin/js/jquery.min.js') }}"></script>
<!-- Bootstrap  -->
<script src = "{{ asset('admin/js/bootstrap.min.js')}}"></script>
<script src = "{{ asset('admin/js/sweetalert-dev.min.js')}}"></script>
<script src = "{{ asset('admin/js/ajax.js')}}"></script>
<script src =  "https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src =  "https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
