<tr class = "item-{{ $employee->id }}">
    <td>{{ $employee->full_name }}</td>
    <td><img width="80" height="80" src="{{ asset('$employee->image') }}"></td>
    <td>{{ substr($employee->job,20) }}</td>
    <td>{{ $employee->User->user_name }}</td>
    <td>{{ $employee->status }}</td>
    <td>
        <button data-id="{{ $employee->id }}" data-route="{{ route('editEmployee',['employeeId'=>$employee->id]) }}" class = "btn btn-default button-custom editEmployee" data-toggle = "modal" data-target = "#editModal">
            Edit
        </button>
        <button style="color: white"  data-id="{{ $employee->id }}" data-route="{{ route('deleteEmployee') }}" class="deleteEmployee btn btn-default btn-danger ">
            delete
        </button>
    </td>
</tr>